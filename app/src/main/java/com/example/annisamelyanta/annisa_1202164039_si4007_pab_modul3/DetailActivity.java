package com.example.annisamelyanta.annisa_1202164039_si4007_pab_modul3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private TextView namaDetail, pekerjaanDetail;
    private ImageView fotoDetail;
    private int avatarKode;
    private String mNama,mPekerjaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        namaDetail = findViewById(R.id.txDetailNama);
        pekerjaanDetail = findViewById(R.id.txDetailPekerjaan);
        fotoDetail = findViewById(R.id.idAvatar);

        mNama = getIntent().getStringExtra("nama");
        mPekerjaan = getIntent().getStringExtra("pekerjaan");
        avatarKode = getIntent().getIntExtra("gender",2);

        namaDetail.setText(mNama);
        pekerjaanDetail.setText(mPekerjaan);

        switch (avatarKode){
            case 1 :
                fotoDetail.setImageResource(R.drawable.ic_man);
                break;
            case 2 :
                fotoDetail.setImageResource(R.drawable.ic_woman);
                break;
        }

    }
}
