package com.example.annisamelyanta.annisa_1202164039_si4007_pab_modul3;

class User {

    private String nama;
    private String pekerjaan;
    private int avatar;

    public User(String nama, String pekerjaan, int avatar) {
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.avatar = avatar;
    }

    public String getNama() {
        return nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public int getAvatar() {
        return avatar;
    }
}
